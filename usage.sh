#!/bin/sh

# License: GPLv3
# https://www.gnu.org/licenses/gpl-3.0.en.html
# Made by Michał Kostrzwski <skyflighter.kos@protonmail.com>
# Gitlab: https://gitlab.com/michaelskyf
# Requirements: dmenu, safeeyes, xdotool

# WARNING: By default this script will turn off eno1 interface, you can change this behaviour by commenting it out


timeFile="$XDG_CACHE_HOME/usage"

currentTime=$(date +%s)
maxTime=2 # in hours

# File structure:
# 1. Seconds since 1970
# 2. Minutes spent on computer
# 3. yes/no for excersises

chmod +w $timeFile

lastTime=$(sed -n 1p $timeFile)

minutes=$(sed -n 2p $timeFile)

Excersise=$(sed -n 3p $timeFile)

if [ $currentTime -gt $lastTime ]
then
	# saveTime gets rounded to days, so we just need to add 1 to get next reset date
	minutes=0
	saveTime=$((currentTime/60/60/24 + 1))
	saveTime=$((saveTime*24*60*60 - 7200))
	sed -i -e "1s/.*$/$saveTime/" $timeFile
	# also set workout to no
	sed -i -e "3s/.*$/no/" $timeFile
fi

minutes=$((minutes+1))
sed -i -e "2s/.*$/$minutes/" $timeFile

hours=$((minutes/60))
minutes=$((minutes%60))

# if safeeyes is running, do not restart
if ! [ "$(pgrep safeeyes)" ]
then
safeeyes &
notify-send "Starting safeeyes"
fi

if [ "$(pgrep safeeyes)" ]
then
	sleep 1
	if [ "$(safeeyes --status)" == "Disabled until restart" ]
	then
		notify-send "Enabling safeeyes"
		safeeyes --enable
		safeeyes -t &
	fi

	if [ "$Excersise" != "yes" ]
	then
		case $( printf "No\nYes" | dmenu -i -p "Have you wokrked out for 10 minutes?") in
			"Yes") Excersise="yes"; sed -i -e "3s/.*$/$Excersise/" $timeFile;;
		esac

	fi
fi

if [ $hours -ge $maxTime ]
then
	sudo ip link set eno1 down
fi

chmod -w $timeFile

# if x hours passed or if it's after xPM or it's before xAM
if [ $hours -ge $maxTime ] || [ $(date +%H) -ge 20 ] || [ $(date +%H) -le 5 ]
then
	notify-send "Turn off computer"
	dmenu -p "Turn off computer" &
	echo "❗❗❗$hours HOURS, $minutes MINUTES❗❗❗"
	if ! [ "$(xdotool search --name --onlyvisible safeeyes)" ] && [ "$(pgrep safeeyes)" ]
	then
		safeeyes -t &
	fi
else
	echo "$hours hours, $minutes minutes"
fi
